# Import des librairies


from flask import Flask, render_template, request, redirect, url_for, session, abort , g
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import pymysql
import flask_pymysql
import marshmallow_sqlalchemy
from flask_restful import Api, Resource
#from werkzeug.security import generate_password_hash, check_password_hash
#from passlib.hash import sha256_crypt
from datetime import datetime
from flask_login import LoginManager, UserMixin, login_user , logout_user , current_user , login_required
from sqlalchemy import func
from werkzeug.security import generate_password_hash, check_password_hash

# Config de l'app
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost/uf-web-b2b?charset=utf8mb4'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-secret'
app.config['SECURITY_REGISTERABLE'] = True


# Instanciation de l'api, la base etc...
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

# Instanciation des variables pour le système de connexion
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


# Décorateur pour charger l'utilisateur
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


# Table de notre base de données
# Table Rôle
class Role(db.Model):
    id_role = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


# Table User
class User(db.Model, UserMixin):
    id_user = db.Column(db.Integer, primary_key=True)
    email = db.Column('email', db.String(255),unique=True)
    password = db.Column('password', db.String(255))
    register_on = db.Column('register_on', db.DateTime)
    commande = db.relationship("Commande")
    commentaire = db.relationship("Commentaire")
    #restaurant = db.relationship("Restaurant")
    id_role = db.Column(db.Integer, db.ForeignKey('role.id_role'))
    role = db.relationship('Role', foreign_keys=[id_role])

    def __init__(self, email, password, id_role):
        self.email = email
        self.password = password
        self.id_role = id_role
        self.register_on = datetime.utcnow()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id_user)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User %r>' % (self.email)


# Table Restaurant
class Restaurant(db.Model):
    id_restaurant = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    description = db.Column(db.String(1200))
    categorie = db.Column(db.String(50))
    code_postal = db.Column(db.Integer)
    ville = db.Column(db.String(50))
    rue = db.Column(db.String(200))
    telephone = db.Column(db.Integer)
    photo = db.Column(db.String(200))
    #id_user = db.Column(db.Integer, db.ForeignKey('user.id_user'))
    commandes = db.relationship("Commande")
    commentaires = db.relationship("Commentaire")
    plat = db.relationship("Plat")
    id_user = db.Column(db.Integer, db.ForeignKey('user.id_user'))
    user = db.relationship('User', foreign_keys=[id_user])
    #restaurant = db.relationship('User', foreign_keys=[id_user])
    #restaurateur = db.relationship("Restaurateur", backref="restaurateur")

    def __repr__(self):
        return '<Restaurant %s>' % self.nom


# Table Plat
class Plat(db.Model):
    id_plat = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    description = db.Column(db.String(1200))
    categorie = db.Column(db.String(50))
    photo = db.Column(db.String(1200))
    prix = db.Column(db.Integer)
    id_restaurant = db.Column(db.Integer, db.ForeignKey('restaurant.id_restaurant'))

    def __init__(self, nom, description, categorie, photo, prix, id_restaurant):
        self.nom = nom
        self.description = description
        self.categorie = categorie
        self.photo = photo
        self.prix = prix
        self.id_restaurant = id_restaurant

    def __repr__(self):
        return '<Plat %s>' % self.nom


# Table Panier
class Panier(db.Model):
    id_panier = db.Column(db.Integer, primary_key=True)
    nom_plat = db.Column(db.String(100))
    prix_plat = db.Column(db.Integer)
    id_restaurant = db.Column(db.Integer, db.ForeignKey('restaurant.id_restaurant'))
    id_user = db.Column(db.Integer, db.ForeignKey('user.id_user'))
    id_plat = db.Column(db.Integer, db.ForeignKey('plat.id_plat'))

    def __init__(self, nom_plat, prix_plat, id_restaurant, id_user, id_plat):
        self.nom_plat = nom_plat
        self.prix_plat = prix_plat
        self.id_restaurant = id_restaurant
        self.id_user = id_user
        self.id_plat = id_plat

    def __repr__(self):
        return '<Panier %s>' % self.nom_plat


# Table Commentaire
class Commentaire(db.Model):
    id_commentaire = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.Integer)
    description = db.Column(db.String(240))
    id_user = db.Column(db.Integer, db.ForeignKey('user.id_user'))
    id_restaurant = db.Column(db.Integer, db.ForeignKey('restaurant.id_restaurant'))

    def __init__(self, id_commentaire, note, description, id_restaurant):
        self.id_commentaire = id_commentaire
        self.note = note
        self.description = description
        self.id_restaurant = id_restaurant

    def __repr__(self):
        return '<Commentaire %s>' % self.description


# Table Commande
class Commande(db.Model):
    id_commande = db.Column(db.Integer, primary_key=True)
    statut = db.Column(db.String(50))
    nom_plat = db.Column(db.String(100))
    prix = db.Column(db.Integer)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id_user'))
    id_restaurant = db.Column(db.Integer, db.ForeignKey('restaurant.id_restaurant'))

    def __init__(self, statut, nom_plat, prix, id_restaurant, id_user):
        self.statut = statut
        self.nom_plat = nom_plat
        self.prix = prix
        self.id_restaurant = id_restaurant
        self.id_user = id_user

    def __repr__(self):
        return '<Commande %s>' % self.statut


# Schéma basé sur nos tables avec utilisation de la bibliothèque Marshmallow pour faire notre API
# Schéma Restaurant
class RestaurantSchema(ma.Schema):
    class Meta:
        fields = ("nom", "description", "categorie", "code_postal", "ville", "rue", "telephone", "photo")

restaurant_schema = RestaurantSchema()
restaurants_schema = RestaurantSchema(many=True)


# Schéma Plat
class PlatSchema(ma.Schema):
    class Meta:
        fields = ("nom", "description", "categorie", "photo", "prix", "plat_commandes", "id_restaurant")

plat_schema = PlatSchema()
plats_schema = PlatSchema(many=True)


# Schéma Panier
class PanierSchema(ma.Schema):
    class Meta:
        field = ("nom_plat", "prix_plat", "id_restaurant", "id_user", "id_plat")

panier_schema = PanierSchema()
paniers_schema = PanierSchema(many=True)


# Schéma Commentaire
class CommentaireSchema(ma.Schema):
    class Meta:
        fields = ("note", "description", "id_restaurant")

commentaire_schema = CommentaireSchema()
commentaires_schema = CommentaireSchema(many=True)


# Schéma Commande
class CommandeSchema(ma.Schema):
    class Meta:
        fields = ("statut", "nom_plat", "prix", "plat_commande", "id_restaurant", "id_user")

commande_schema = CommandeSchema()
commandes_schema = CommandeSchema(many=True)


# RESTful resources (CRUD) pour les API
# CRUD Restaurant
class RestaurantListResource(Resource):
    def get(self):
        restaurants = Restaurant.query.all()
        return restaurants_schema.dump(restaurants)

    def post(self):
        new_restaurant = Restaurant(
            nom=request.json['nom'],
            description=request.json['description'],
            categorie=request.json['categorie'],
            code_postal=request.json['code_postal'],
            ville=request.json['ville'],
            rue=request.json['rue'],
            telephone=request.json['telephone'],
            photo=request.json['photo']
        )
        db.session.add(new_restaurant)
        db.session.commit()
        return restaurant_schema.dump(new_restaurant)

api.add_resource(RestaurantListResource, '/test-restaurants')


class RestaurantResource(Resource):
    def get(self, restaurant_id):
        restaurant = Restaurant.query.get_or_404(restaurant_id)
        return restaurant_schema.dump(restaurant)

    def patch(self, restaurant_id):
        restaurant = Restaurant.query.get_or_404(restaurant_id)

        if 'nom' in request.json:
            restaurant.nom = request.json['nom']
        if 'description' in request.json:
            restaurant.description = request.json['description']
        if 'categorie' in request.json:
            restaurant.categorie = request.json['categorie']
        if 'code_postal' in request.json:
            restaurant.adresse_postale = request.json['code_postal']
        if 'ville' in request.json:
            restaurant.ville = request.json['ville']
        if 'rue' in request.json:
            restaurant.rue = request.json['rue']
        if 'telephone' in request.json:
            restaurant.telephone = request.json['telephone']
        if 'photo' in request.json:
            restaurant.photo = request.json['photo']

        db.session.commit()
        return restaurant_schema.dump(restaurant)

    def delete(self, restaurant_id):
        restaurant = Restaurant.query.get_or_404(restaurant_id)
        db.session.delete(restaurant)
        db.session.commit()
        return '', 204


api.add_resource(RestaurantResource, '/test-restaurants/<int:restaurant_id>')

# CRUD Plat
class PlatListResource(Resource):
    def get(self):
        plats = Plat.query.all()
        return plats_schema.dump(plats)

    def post(self):
        new_plat = Plat(
            nom=request.json['nom'],
            description=request.json['description'],
            categorie=request.json['categorie'],
            photo=request.json['photo'],
            prix=request.json['prix'],
            id_restaurant=request.json['id_restaurant']
        )
        db.session.add(new_plat)
        db.session.commit()
        return plat_schema.dump(new_plat)

api.add_resource(PlatListResource, '/test-plats')


class PlatResource(Resource):
    def get(self, plat_id):
        plat = Plat.query.get_or_404(plat_id)
        return plat_schema.dump(plat)

    def patch(self, plat_id):
        plat = Plat.query.get_or_404(plat_id)
        if 'nom' in request.json:
            plat.nom = request.json['nom']
        if 'description' in request.json:
            plat.description = request.json['description']
        if 'categorie' in request.json:
            plat.categorie = request.json['categorie']
        if 'photo' in request.json:
            plat.photo = request.json['photo']
        if 'prix' in request.json:
            plat.prix = request.json['prix']
        if 'id_restaurant' in request.json:
            plat.id_restaurant = request.json['id_restaurant']

        db.session.commit()
        return plat_schema.dump(plat)

    def delete(self, plat_id):
        plat = Plat.query.get_or_404(plat_id)
        db.session.delete(plat)
        db.session.commit()
        return '', 204


api.add_resource(PlatResource, '/test-plats/<int:plat_id>')


# CRUD Panier
class PanierListResource(Resource):
    def get(self):
        paniers = Panier.query.all()
        return paniers_schema.dump(paniers)

    def post(self):
        new_panier = Panier(
            nom_plat=request.json['nom_plat'],
            prix_plat=request.json['prix_plat'],
            id_restaurant=request.json['id_restaurant'],
            id_user=request.json['id_user'],
            id_plat=request.json['id_plat']
        )
        db.session.add(new_panier)
        db.session.commit()
        return panier_schema.dump(new_panier)

api.add_resource(PanierListResource, '/test-paniers')


class PanierResource(Resource):
    def get(self, panier_id):
        panier = Panier.query.get_or_404(panier_id)
        return panier_schema.dump(panier)

    def patch(self, panier_id):
        panier = Panier.query.get_or_404(panier_id)
        if 'nom_plat' in request.json:
            panier.nom_plat = request.json['nom_plat']
        if 'prix_plat' in request.json:
            panier.prix_plat = request.json['prix_plat']
        if 'id_restaurant' in request.json:
            panier.id_restaurant = request.json['id_restaurant']
        if 'id_user' in request.json:
            panier.id_user = request.json['id_user']
        if 'id_plat' in request.json:
            panier.id_plat = request.json['id_plat']
        db.session.commit()
        return panier_schema.dump(panier)

    def delete(self, panier_id):
        panier = Panier.query.get_or_404(panier_id)
        db.session.delete(panier)
        db.session.commit()
        return '', 204


api.add_resource(PanierResource, '/test-paniers/<int:panier_id>')


# CRUD Commentaire
class CommentaireListResource(Resource):
    def get(self):
        commentaires = Commentaire.query.all()
        return commentaires_schema.dump(commentaires)

    def post(self):
        new_commentaire = Commentaire(
            id_commentaire=request.json['id_commentaire'],
            note=request.json['note'],
            description=request.json['description'],
            id_restaurant=request.json['id_restaurant']
        )
        db.session.add(new_commentaire)
        db.session.commit()
        return commentaire_schema.dump(new_commentaire)

api.add_resource(CommentaireListResource, '/test-commentaires')


class CommentaireResource(Resource):
    def get(self, commentaire_id):
        commentaire = Commentaire.query.get_or_404(commentaire_id)
        return commentaire_schema.dump(commentaire)

    def patch(self, commentaire_id):
        commentaire = Commentaire.query.get_or_404(commentaire_id)
        if 'id_commentaire' in request.json:
            commentaire.id_commentaire = request.json['id_commentaire']
        if 'note' in request.json:
            commentaire.note = request.json['note']
        if 'description' in request.json:
            commentaire.description = request.json['description']
        if 'id_restaurant' in request.json:
            commentaire.id_restaurant = request.json['id_restaurant']

        db.session.commit()
        return commentaire_schema.dump(commentaire)

    def delete(self, commentaire_id):
        commentaire = Commentaire.query.get_or_404(commentaire_id)
        db.session.delete(commentaire)
        db.session.commit()
        return '', 204


api.add_resource(CommentaireResource, '/test-commentaires/<int:commentaire_id>')


# CRUD Commande
class CommandeListResource(Resource):
    def get(self):
        commandes = Commande.query.all()
        return commandes_schema.dump(commandes)

    def post(self):
        new_commande = Commande(
            statut=request.json['statut'],
            nom_plat=request.json['nom_plat'],
            prix=request.json['prix'],
            id_restaurant=request.json['id_restaurant'],
            id_user=request.json['id_user']
        )
        db.session.add(new_commande)
        db.session.commit()
        return commande_schema.dump(new_commande)

api.add_resource(CommandeListResource, '/test-commandes')


class CommandeResource(Resource):
    def get(self, commande_id):
        commande = Commande.query.get_or_404(commande_id)
        return commande_schema.dump(commande)

    def patch(self, commande_id):
        commande = Commande.query.get_or_404(commande_id)
        if 'id_commande' in request.json:
            commande.id_commande = request.json['id_commande']
        if 'statut' in request.json:
            commande.statut = request.json['statut']
        if 'nom_plat' in request.json:
            commande.nom_plat = request.json['nom_plat']
        if 'prix' in request.json:
            commande.prix = request.json['prix']
        if 'id_restaurant' in request.json:
            commande.id_restaurant = request.json['id_restaurant']
        if 'id_user' in request.json:
            commande.id_user = request.json['id_user']

        db.session.commit()
        return commande_schema.dump(commande)

    def delete(self, commande_id):
        commande = Commande.query.get_or_404(commande_id)
        db.session.delete(commande)
        db.session.commit()
        return '', 204


api.add_resource(CommandeResource, '/test-commandes/<int:commande_id>')


# Toutes nos routes
# Page d'accueil
@app.route('/', methods=['GET'])
def index():
    restaurant_partenaire = Restaurant.query.all()
    restaurants = Restaurant.query.order_by(func.random()).limit(8).all()
    countRestaurants = Restaurant.query.count()
    return render_template("index.html", restaurants=restaurants, restaurant_partenaire=restaurant_partenaire, countRestaurants=countRestaurants)


# Page pour s'inscrire
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        #print(request.form)
        user = User(email=request.form['email'], password=generate_password_hash(request.form['password']), id_role=int(request.form['role']))
        db.session.add(user)
        db.session.commit()
        #print("L'utilisateur à été créer avec succès")
        return redirect(url_for('login'))
    return render_template('register.html')


# Page pour se connecter
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    if request.method == 'POST':
        user = User.query.filter_by(email=request.form['email']).first()
        if user is None or not user.check_password(password=request.form['password']):
            #print("Nom d'utilisateur ou mot de passe incorrect")
            return redirect(url_for('login'))
        #print("Tu es connecté")
        login_user(user)
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In')


# Page pour se déconnecter
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


# Cette fonction sert à "traquer" l'utilisateur actuel sur le site
@app.before_request
def before_request():
    #print("current_user qui est entrain de naviguer sur le site")
    g.user = current_user


# Page pour créer son restaurant
@app.route('/get_restaurant', methods=['GET'])
@login_required
def get_restaurant():
    return render_template('post_restaurant.html')


# La route qui créer ton restaurant
@app.route('/post_restaurant', methods=['POST'])
@login_required
def post_restaurant():
    restaurant = Restaurant(
        nom=request.form['nom'],
        description=request.form['description'],
        categorie=request.form['categorie'],
        code_postal=request.form['code_postal'],
        ville=request.form['ville'],
        rue=request.form['rue'],
        telephone=request.form['telephone'],
        photo=request.form['photo'],
        id_user=int(current_user.id_user))

    db.session.add(restaurant)
    db.session.commit()
    return redirect(url_for('index'))


# La page qui te demande dans quel restaurant tu souhaites créer ton plat
@app.route('/plat', methods=['GET'])
@login_required
def plat():
    restaurants = Restaurant.query.filter(Restaurant.id_user == current_user.id_user).all()
    return render_template("plat.html", restaurants=restaurants)


# La page pour créer ton plat selon le restaurant que vous avez choisi (avec l'id)
@app.route('/get_plat/<int:id_restaurant>', methods=['GET'])
@login_required
def get_plat(id_restaurant):
    peter = Restaurant.query.filter_by(id_restaurant=id_restaurant).first()
    return render_template('post_plat.html', restaurant=peter)


# La route pour créer les plats selon le restaurant que vous avez choisi (avec l'id)
@app.route('/post_plat/<int:id_restaurant>', methods=['POST'])
@login_required
def post_plat(id_restaurant):
    peter = Restaurant.query.filter_by(id_restaurant=id_restaurant).first()
    plat = Plat(
        nom=request.form['nom'],
        description=request.form['description'],
        categorie=request.form['categorie'],
        photo=request.form['photo'],
        prix=request.form['prix'],
        id_restaurant=peter.id_restaurant
    )
    db.session.add(plat)
    db.session.commit()
    return redirect(url_for('index'))


# Page pour afficher le contenu de ton panier
@app.route('/panier', methods=['GET'])
@login_required
def get_panier():
    panier = Panier.query.filter_by(id_user=current_user.id_user).all()
    return render_template("panier.html", panier=panier)


# La route qui sert à ajouter les plats dans le panier
@app.route('/post_panier/<int:id_plat>/<int:id_user>', methods=['POST'])
@login_required
def post_panier(id_plat, id_user):
    plat = Plat.query.filter_by(id_plat=id_plat).first()
    user = User.query.filter_by(id_user=id_user).first()
    add_basket = Panier(
        nom_plat=plat.nom,
        prix_plat=plat.prix,
        id_restaurant=plat.id_restaurant,
        id_plat=plat.id_plat,
        id_user=user.id_user
    )
    db.session.add(add_basket)
    db.session.commit()
    return redirect(url_for('restaurants'))


# La route pour supprimer un plat du panier
@app.route('/panier/<int:id_panier>', methods=['POST'])
@login_required
def delete_panier(id_panier):
    supprimer_panier = Panier.query.filter_by(id_panier=id_panier).first()
    db.session.delete(supprimer_panier)
    db.session.commit()
    return redirect(url_for('get_panier'))


# La route qui sert à valider ta commande et supprimer le plat du panier
@app.route('/post_commande/<int:id_panier>', methods=['POST'])
@login_required
def post_commande(id_panier):
    panier = Panier.query.filter_by(id_panier=id_panier).first()
    commande = Commande(
        statut="En cours",
        nom_plat=panier.nom_plat,
        prix=panier.prix_plat,
        id_user=panier.id_user,
        id_restaurant=panier.id_restaurant
    )
    suppr_panier = Panier.query.filter_by(id_panier=id_panier).delete()
    db.session.add(commande, suppr_panier)
    db.session.commit()
    return redirect(url_for('get_panier'))


# La page pour afficher tout les restaurants
@app.route('/restaurants',  methods=['GET'])
def restaurants():
    restaurants = Restaurant.query.all()
    return render_template("allRestaurants.html", restaurants=restaurants)


# La page pour afficher le restaurant sur lequel vous avez cliquez
@app.route('/restaurant/<int:id_restaurant>')
@login_required
def restaurant(id_restaurant):
    restaurant = Restaurant.query.filter(Restaurant.id_restaurant == id_restaurant).all()
    plat = Plat.query.filter(Plat.id_restaurant == id_restaurant).all()
    panier = Panier.query.filter(Panier.id_user == current_user.id_user).all()
    return render_template("restaurant.html", restaurant=restaurant, plat=plat, panier=panier)


# La page pour afficher les informations liées à ton compte (la page de profile)
@app.route('/profil/<int:id_user>', methods=['GET'])
@login_required
def profil(id_user):
    user = User.query.filter(User.id_user == id_user).all()
    profil_user = User.query.filter_by(id_user=id_user).first()
    restaurants = Restaurant.query.filter(Restaurant.id_user == id_user).all()
    commandes = Commande.query.filter(Commande.id_user == id_user).all()
    return render_template('profil.html', user=user, profil_user=profil_user, restaurants=restaurants, commandes=commandes)


# La page pour afficher les informations du ou des restaurants que vous possédez
@app.route('/profil/<int:id_user>/<int:id_restaurant>')
@login_required
def profil_restaurant(id_restaurant, id_user):
    restaurant = Restaurant.query.filter_by(id_restaurant=id_restaurant).all()
    profil_restaurateur = User.query.filter_by(id_user=id_user).first()
    commandes = Commande.query.filter(Commande.id_user == id_restaurant).all()
    plats = Plat.query.filter(Plat.id_restaurant == id_restaurant).all()
    return render_template('profil_restaurant.html', restaurant=restaurant, profil_restaurateur=profil_restaurateur, commandes=commandes, plats=plats)


# La route qui permet de mettre à jour le statut de ta commande (La commande peut passer du statut "envoyée" à "clôturée")
@app.route('/profil/commande/<id_user>/<id_restaurant>/<int:id_commande>/update', methods=['POST'])
@login_required
def update_commande(id_commande, id_user, id_restaurant):
    commande = Commande.query.filter_by(id_commande=id_commande).first()
    newstatut = request.form.get("newstatut")
    commande.statut = newstatut
    db.session.commit()
    return redirect('/profil/' + id_user + '/' + id_restaurant)


# La page principale du Dashboard admin
@app.route('/admin/')
@login_required
def admin():
    countRestaurants = Restaurant.query.count()
    countUsers = User.query.count()
    countCommandes = Commande.query.count()
    countBenefices = (Commande.query.count())*3
    commandes = Commande.query.order_by(Commande.id_commande.desc()).all()
    return render_template("admin/adminBoard.html", countRestaurants=countRestaurants, countUsers=countUsers, countCommandes=countCommandes, countBenefices=countBenefices, commandes=commandes)


# La page pour afficher tout les utilisateurs du site
@app.route('/admin/users')
@login_required
def adminAllUsers():
    users = User.query.all()
    return render_template("admin/adminAllUsers.html", users=users)


# La route qui sert à supprimer un utilisateur
@app.route('/admin/users/delete', methods=['GET', 'POST'])
@login_required
def adminUsersDelete():
    if request.form:
        user = User(
            email=request.form.get("email"),
            password=request.form.get("password"),
            id_role=request.form.get("id_role")
        )
        db.session.add(user)
        db.session.commit()
    users = User.query.all()
    return render_template("admin/adminAllUsers.html", users=users)


# La page pour afficher tout les restaurants créés sur le site
@app.route('/admin/restaurants')
@login_required
def adminAllRestaurants():
    restaurants = Restaurant.query.all()
    return render_template("admin/adminAllRestaurants.html", restaurants=restaurants)


# La page pour afficher les informations d'un utilisateur en particulié
@app.route('/admin/users/<int:id_user>')
@login_required
def adminUser(id_user):
    user = User.query.filter(User.id_user == id_user).all()
    restaurants = Restaurant.query.filter(Restaurant.id_user == id_user).all()
    commandes = Commande.query.filter(Commande.id_user == id_user).all()
    return render_template("admin/adminUser.html", user=user, commandes=commandes, restaurants=restaurants)


# La page pour afficher les informations d'un restaurant en particulié
@app.route('/admin/restaurants/<int:id_restaurant>')
@login_required
def adminRestaurant(id_restaurant):
    restaurant = Restaurant.query.filter(Restaurant.id_restaurant == id_restaurant).all()
    commandes = Commande.query.filter(Commande.id_user == id_restaurant).all()
    plats = Plat.query.filter(Plat.id_plat == id_restaurant).all()
    return render_template("admin/adminRestaurant.html", restaurant=restaurant, commandes=commandes, plats=plats)


# Démarrer l'application
if __name__ == '__main__':
    app.run(debug=True)